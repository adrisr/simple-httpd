#ifndef LOG_H_INCLUDED
#define LOG_H_INCLUDED

#include <stdio.h>
#include <unistd.h>

#define LOG(MSG, ARGS...) fprintf(stderr, "%s [%s:%d] " MSG "\n", log_timestamp(), log_name(), getpid(), ## ARGS)

const char *log_timestamp();
const char *log_name();
void log_set_name(const char *name);

#endif /* LOG_H_INCLUDED */
