TARGET=httpserver
CFLAGS=-Wall -std=c99 -g -D_POSIX_SOURCE
LDFLAGS=
OBJECTS=server.o \
        request.o \
        log.o

default: all

all: $(TARGET)

$(TARGET): $(OBJECTS)
	$(CC) $(LDFLAGS) $^ -o $@

%.o: %.c
	$(CC) -c $(CFLAGS) $^ -o $@

.PHONY: clean

clean:
	rm -f $(TARGET) *.o
