#ifndef REQUEST_H_INCLUDED
#define REQUEST_H_INCLUDED

void handle_request(int socket);
int close_fd(int fd);

#endif /* REQUEST_H_INCLUDED */
