#ifndef CONFIG_H_INCLUDED
#define CONFIG_H_INCLUDED

#define BACKLOG 8

#define LISTEN_PORT 8080

#define MAX_REQUEST_SIZE 2048

#define MAX_RESPONSE_HEADERS_SIZE 512

#define BASE_DIR "./"

/* maximum length for a request METHOD (GET, POST, etc.) before
   it is considered a wrong request */
#define MAX_METHOD_LENGTH 8

#define MAX_PATH_LENGTH 1024

#define DEFAULT_CONTENT_TYPE "application/octet-stream"

#endif /* CONFIG_H_INCLUDED */
