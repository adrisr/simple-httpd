#include "log.h"

#include <string.h>
#include <sys/types.h>
#include <time.h>

#define MAX_TIME_LEN 64
#define MAX_NAME_LEN 32

static char name[MAX_NAME_LEN];

const char *log_name() {
    return name;
}

const char *log_timestamp() {
    static char buf[MAX_TIME_LEN];

    time_t t = time(NULL);
    struct tm *tm = localtime(&t);

    if(strftime(buf, MAX_TIME_LEN, "%c", tm)) {
        return buf;
    }
    return "";
}

/* set the log identifier for this process */
void log_set_name(const char *s) {
    strncpy(name, s, MAX_NAME_LEN);
    name[MAX_NAME_LEN-1] = '\0';
}

